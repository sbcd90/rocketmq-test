package org.apache.rocketmq.batch;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BatchProducer {

  public static void main(String[] args) throws Exception {

    DefaultMQProducer producer = new DefaultMQProducer("batch-producer");
    producer.setNamesrvAddr("localhost:9876");

    producer.start();

    List<Message> messages = new ArrayList<>();

    Message message1 = new Message("BatchTopic", "OrderID01", "Hello World1".getBytes(RemotingHelper.DEFAULT_CHARSET));
    Message message2 = new Message("BatchTopic", "OrderID02", "Hello World2".getBytes(RemotingHelper.DEFAULT_CHARSET));
    Message message3 = new Message("BatchTopic", "OrderID03", "Hello World3".getBytes(RemotingHelper.DEFAULT_CHARSET));
    Message message4 = new Message("BatchTopic", "OrderID04", "Hello World4".getBytes(RemotingHelper.DEFAULT_CHARSET));

    messages.addAll(Arrays.asList(message1, message2, message3, message4));

    producer.send(messages);
    producer.shutdown();
  }
}