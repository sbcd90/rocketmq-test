package org.apache.rocketmq.broadcast;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;

public class BroadcastConsumer {

  public static void main(String[] args) throws Exception {

    DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("broadcast-consumer");
    consumer.setNamesrvAddr("localhost:9876");

    // set to broadcast mode
    consumer.setMessageModel(MessageModel.BROADCASTING);
    consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);

    consumer.subscribe("BroadcastTopic", "TagA || TagB || TagC");
    consumer.registerMessageListener(new MessageListenerConcurrently() {
      int totalNoOfMsgs = 0;

      @Override
      public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
                                                      ConsumeConcurrentlyContext context) {
        totalNoOfMsgs += msgs.size();
        System.out.println(Thread.currentThread().getName() + " Receive New Messages: " + totalNoOfMsgs +
          " Current Timestamp: " + System.currentTimeMillis());
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
      }
    });

    consumer.start();
  }
}