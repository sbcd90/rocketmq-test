package org.apache.rocketmq.broadcast;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class BroadcastProducer {

  public static void main(String[] args) throws Exception {

    DefaultMQProducer producer = new DefaultMQProducer("broadcast-producer");
    producer.setNamesrvAddr("localhost:9876");

    producer.start();

    for (int i = 1;i <= 100;i++) {
      Message message = new Message("BroadcastTopic",
        "TagA", "BroadcastId188", "Hello World".getBytes(RemotingHelper.DEFAULT_CHARSET));

      SendResult sendResult = producer.send(message);
      System.out.println(sendResult);
    }
    producer.shutdown();
  }
}