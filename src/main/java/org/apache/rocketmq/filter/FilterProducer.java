package org.apache.rocketmq.filter;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class FilterProducer {

  public static void main(String[] args) throws Exception {

    DefaultMQProducer producer = new DefaultMQProducer("filterProducer");
    producer.setNamesrvAddr("localhost:9876");
    producer.start();

    for (int i = 0;i < 100;i++) {
      Message message = new Message("FilterTopic",
        "TagA",
        ("Hello World" + i).getBytes(RemotingHelper.DEFAULT_CHARSET));

      message.putUserProperty("a", String.valueOf(i));

      producer.send(message);
    }
    producer.shutdown();
  }
}