package org.apache.rocketmq.open.messaging;

import io.openmessaging.*;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class OpenMessagingProducer {

  public static void main(String[] args) throws Exception {
    final MessagingAccessPoint messagingAccessPoint = MessagingAccessPointFactory
      .getMessagingAccessPoint("openmessaging:rocketmq://localhost:9876/namespace");

    final Producer producer = messagingAccessPoint.createProducer();

    messagingAccessPoint.startup();
    System.out.println("MessagingAccessPoint startup OK");

    producer.startup();

    /**
     * Normal message delivery with response as messageId
     */
    Message message = producer.createBytesMessageToTopic("OMS_TOPIC",
      "Hello World1".getBytes(RemotingHelper.DEFAULT_CHARSET));
    SendResult sendResult = producer.send(message);
    System.out.println("Message sent with Id: " + sendResult.messageId());

    /**
     * Async message delivery
     */
    final Promise<SendResult> result = producer.sendAsync(producer.createBytesMessageToTopic("OMS_TOPIC",
      "Hello World2".getBytes(RemotingHelper.DEFAULT_CHARSET)));
    result.addListener(new PromiseListener<SendResult>() {
      @Override
      public void operationCompleted(Promise<SendResult> promise) {
        System.out.println("Async message sent with Id: " + promise.get().messageId());
      }

      @Override
      public void operationFailed(Promise<SendResult> promise) {
        System.out.println("Async message failed with error: " + promise.getThrowable().getMessage());
      }
    });

    /**
     * One-way messages
     */
    producer.sendOneway(producer.createBytesMessageToTopic("OMS_TOPIC",
      "Hello World4".getBytes(RemotingHelper.DEFAULT_CHARSET)));
    System.out.println("Send one-way message OK");

    producer.shutdown();
    messagingAccessPoint.shutdown();
  }
}