package org.apache.rocketmq.open.messaging;

import io.openmessaging.*;
import io.openmessaging.rocketmq.domain.NonStandardKeys;

public class OpenMessagingPullConsumer {

  public static void main(String[] args) {
    final MessagingAccessPoint messagingAccessPoint = MessagingAccessPointFactory
      .getMessagingAccessPoint("openmessaging:rocketmq://localhost:9876/namespace");

    final PullConsumer consumer = messagingAccessPoint.createPullConsumer("OMS_TOPIC",
      OMS.newKeyValue().put(NonStandardKeys.CONSUMER_GROUP, "OMS_CONSUMER"));

    messagingAccessPoint.startup();
    System.out.println("MessagingAccessPoint startup OK");

    consumer.startup();
    System.out.println("Consumer startup OK");

    Message message = consumer.poll();
    if (message != null) {
      String msgId = message.headers().getString(MessageHeader.MESSAGE_ID);
      System.out.println("Message received with Id: " + msgId);
      consumer.ack(msgId);
    }

    consumer.shutdown();
    messagingAccessPoint.shutdown();
  }
}