package org.apache.rocketmq.ordered;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.List;
import java.util.Random;

public class OrderedRocketMQProducer {

  public static void main(String[] args) throws Exception {

    DefaultMQProducer producer = new DefaultMQProducer("OrderedGroup");
    producer.setNamesrvAddr("localhost:9876");

    producer.start();
    String[] tags = new String[]{"TagA", "TagB", "TagC", "TagD", "TagE"};

    int orderId = 1;

    Message message = new Message("OrderedTopic", tags[new Random().nextInt(4)],
      "KEY" + new Random().nextInt(4), "Hello World".getBytes());

    SendResult sendResult = producer.send(message, new MessageQueueSelector() {
      @Override
      public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
        Integer id = (Integer) arg;
        int index = id % mqs.size();
        return mqs.get(index);
      }
    }, orderId);

    System.out.println(sendResult);

    producer.shutdown();
  }
}