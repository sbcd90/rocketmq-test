package org.apache.rocketmq.scheduling;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class SchedulingConsumer {

  public static void main(String[] args) throws Exception {

    DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("scheduled-consumer");
    consumer.setNamesrvAddr("localhost:9876");
    consumer.subscribe("ScheduledTopic", "*");

    consumer.registerMessageListener(new MessageListenerConcurrently() {
      @Override
      public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
                                                      ConsumeConcurrentlyContext context) {
        for (MessageExt msg: msgs) {
          System.out.println("Receive message[msgId=" + msg.getMsgId() + "] " +
            (System.currentTimeMillis() - msg.getStoreTimestamp()) + " ms later");
        }
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
      }
    });
    consumer.start();
  }
}