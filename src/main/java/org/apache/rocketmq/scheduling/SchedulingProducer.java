package org.apache.rocketmq.scheduling;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class SchedulingProducer {

  public static void main(String[] args) throws Exception {

    DefaultMQProducer producer = new DefaultMQProducer("scheduled-producer");
    producer.setNamesrvAddr("localhost:9876");

    producer.start();
    int totalMsgsToSend = 100;
    for (int i = 0;i < totalMsgsToSend;i++) {
      Message message = new Message("ScheduledTopic",
        ("Hello World" + i).getBytes(RemotingHelper.DEFAULT_CHARSET));
      message.setDelayTimeLevel(3);

      producer.send(message);
    }

    producer.shutdown();
  }
}