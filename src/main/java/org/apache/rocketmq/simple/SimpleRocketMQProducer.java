package org.apache.rocketmq.simple;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

public class SimpleRocketMQProducer {

  public static void main(String[] args) throws Exception {

    DefaultMQProducer producer = new DefaultMQProducer("default-producer");
    producer.setNamesrvAddr("localhost:9876");

    producer.start();

    Message message = new Message("SimpleTopic", "SimpleTag",
      "Hello World".getBytes("UTF-8"));

    SendResult sendResult = producer.send(message);
    System.out.println(sendResult);

    producer.shutdown();
  }
}