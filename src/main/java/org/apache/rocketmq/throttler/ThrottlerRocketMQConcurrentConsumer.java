package org.apache.rocketmq.throttler;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.*;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class ThrottlerRocketMQConcurrentConsumer {

  public static void main(String[] args) throws Exception {

    DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("default-consumer2");
    consumer.setNamesrvAddr("localhost:9876");

    consumer.subscribe("ThrottlerTopic", "*");

    consumer.registerMessageListener(new MessageListenerConcurrently() {
      int totalNoOfMsgs = 0;

      @Override
      public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
                                                      ConsumeConcurrentlyContext context) {
        totalNoOfMsgs += msgs.size();
        System.out.println(Thread.currentThread().getName() + " Receive New Messages: " + totalNoOfMsgs +
          " Current System Time: " + System.currentTimeMillis());
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
      }
    });

    consumer.start();
    System.out.println("Consumer started.");
  }
}