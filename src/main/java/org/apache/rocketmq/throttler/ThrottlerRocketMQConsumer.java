package org.apache.rocketmq.throttler;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class ThrottlerRocketMQConsumer {

  public static void main(String[] args) throws Exception {

    DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("default-consumer1");
    consumer.setNamesrvAddr("localhost:9876");

    consumer.subscribe("ThrottlerTopic", "*");

    consumer.registerMessageListener(new MessageListenerOrderly() {
      int totalNoOfMsgs = 0;

      @Override
      public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs,
                                                 ConsumeOrderlyContext context) {
        totalNoOfMsgs += msgs.size();
        System.out.println(Thread.currentThread().getName() + " Receive New Messages: " + totalNoOfMsgs +
          " Current System Time: " + System.currentTimeMillis());
        return ConsumeOrderlyStatus.SUCCESS;
      }
    });

    consumer.start();
    System.out.println("Consumer started.");
  }
}