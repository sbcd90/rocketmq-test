package org.apache.rocketmq.throttler;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

public class ThrottlerRocketMQProducer {

  public static void main(String[] args) throws Exception {

    DefaultMQProducer producer = new DefaultMQProducer("throttler-producer");
    producer.setNamesrvAddr("localhost:9876");

    producer.start();

    for (int count = 1;count <= 100000;count++) {
      Message message = new Message("ThrottlerTopic", "ThrottlerTag",
        ("Hello World" + count).getBytes("UTF-8"));
      SendResult sendResult = producer.send(message);
      System.out.println(sendResult);
    }

    producer.shutdown();
  }
}